package com.example.productcatalogserviceproxy.Controllers;

import com.example.productcatalogserviceproxy.Dtos.CategoryDto;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/categories")
public class CategoryController {
    @GetMapping("")
    public String getAllCategories() {
        return "All Categories";
    }

    @GetMapping("/{id}")
    public String getAllCategories(@PathVariable String id) {
        return "Category detail - " + id;
    }

    @PostMapping("")
    public String createCategory(@RequestBody CategoryDto categoryDto) {
        return "created category " + categoryDto.getName();
    }

    @PatchMapping("")
    public String patchCategory(@RequestBody CategoryDto categoryDto)
    {
        return "Patched category " + categoryDto.getName() + " :desc-> " + categoryDto.getDescription() + System.getProperty("user.dir");
    }
}
