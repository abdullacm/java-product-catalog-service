package com.example.productcatalogserviceproxy.Controllers;

import com.example.productcatalogserviceproxy.Clients.FakeStore.Dtos.FakeStoreProductDto;
import com.example.productcatalogserviceproxy.Dtos.CategoryDto;
import com.example.productcatalogserviceproxy.Dtos.ProductDto;
import com.example.productcatalogserviceproxy.Models.Category;
import com.example.productcatalogserviceproxy.Models.Product;
import com.example.productcatalogserviceproxy.Services.IProductService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/products")
@RestController
public class ProductController {

    IProductService productService;

    public ProductController(IProductService productService) {
        this.productService = productService;
    }

    @GetMapping("")
    public List<Product> getAllProducts() {
        return productService.getProducts();
    }

    @GetMapping("/{id}")
    public Product getProduct(@PathVariable("id") Long productId) {
        try {
            if (productId <= 0) {
                throw new IllegalArgumentException("Product id is invalid");
            }

            return productService.getProduct(productId);
        } catch (Exception exception) {
            throw exception;
        }
    }

    @PostMapping("")
    public Product createProduct(@RequestBody ProductDto productDto) {
        return productService.createProduct(getProduct(productDto));
    }

    @PatchMapping("/{id}")
    public Product updateProduct(@PathVariable Long id, @RequestBody ProductDto productDto) {
       return productService.updateProduct(id, getProduct(productDto));
    }

    private Product getProduct(ProductDto productDto) {
        Product product = new Product();
        product.setId(productDto.getId());
        product.setTitle(productDto.getTitle());
        product.setPrice(productDto.getPrice());
        product.setDescription(productDto.getDescription());
        product.setImageUrl(productDto.getImage());
        Category category = new Category();
        //category.setName(productDto.getCategory());
        product.setCategory(productDto.getCategory());
        return product;
    }
}
