package com.example.productcatalogserviceproxy.Models;

public enum Status {

    ACTIVE, INACTIVE
}
