package com.example.productcatalogserviceproxy.Clients.FakeStore.Client;

import com.example.productcatalogserviceproxy.Clients.FakeStore.Dtos.FakeStoreProductDto;
import com.example.productcatalogserviceproxy.Models.Product;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class FakeStoreAPIClient {
    RestTemplateBuilder restTemplateBuilder;

    public FakeStoreAPIClient(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplateBuilder = restTemplateBuilder;
    }
    public FakeStoreProductDto getProduct(Long productId)
    {
        RestTemplate restTemplate = this.restTemplateBuilder.build();
        FakeStoreProductDto fakeStoreProductDto = restTemplate.getForEntity("https://fakestoreapi.com/products/{id}", FakeStoreProductDto.class, productId).getBody();
        return fakeStoreProductDto;
    }

    public FakeStoreProductDto createProduct(FakeStoreProductDto fakeStoreProductDto)
    {
        RestTemplate restTemplate = restTemplateBuilder.build();
        return restTemplate.postForEntity("https://fakestoreapi.com/products/", fakeStoreProductDto, FakeStoreProductDto.class).getBody();
    }
}
