package com.example.productcatalogserviceproxy.Clients.FakeStore.Dtos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FakeStoreRatingDto {
    private double rate;
    private Long count;
}
