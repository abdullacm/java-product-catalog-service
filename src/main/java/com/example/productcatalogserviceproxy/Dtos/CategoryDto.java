package com.example.productcatalogserviceproxy.Dtos;

import lombok.Getter;
@Getter

public class CategoryDto {
    private String name;
    private String description;
}
